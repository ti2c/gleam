/*
| Euclean ("enhanced" to JS Math) math utilities.
*/
def.abstract = true;

const abs = Math.abs;
const cos = Math.cos;
const pow = Math.pow;
const sqrt = Math.sqrt;
const pi = Math.PI;

// Sign of number.
const sgn = ( x ) => x < 0 ? -1 : 1;
const sqr3 = sqrt( 3 );

/*
| Based on https://www.particleincell.com/2013/cubic-line-intersection/
|
| ~v3: v^3
| ~v2: v^2
| ~v1: v^1
| ~v0: v^0
*/
def.static.cubicRoots =
	function( v3, v2, v1, v0 )
{
	const a = v2 / v3;
	const b = v1 / v3;
	const c = v0 / v3;

	const q = ( 3*b - a*a ) / 9;
	const r = ( 9*a*b - 27*c - 2*a*a*a ) / 54;
	const d = q*q*q + r*r;

	const result = [ ];
	if( d >= 0 )
	{
		// complex or duplicate roots
		const dsr = sqrt( d );
		const s = sgn( r + dsr ) * pow( abs( r + dsr ), (1/3) );
		const t = sgn( r - dsr ) * pow( abs( r - dsr ), (1/3) );

		const r0 = -a / 3 + ( s + t );
		if( r0 >= 0 && r0 <= 1 ) result.push( r0 );

		if( sqr3 * ( s - t ) / 2 === 0 )
		{
			const r12 = -a/3 - ( s + t ) / 2;
			if( r12 >= 0 && r12 <= 1 ) result.push( r12, r12 );
		}
	}
	else
	{
		// distinct real roots
		const th = Math.acos( r / sqrt( -q*q*q ) );
		const qsr2 = 2 * sqrt( -q );
		const a03 = a / 3;

		const r0 = qsr2 * cos( th / 3 ) - a03;
		const r1 = qsr2 * cos( ( th + 2*pi ) / 3 ) - a03;
		const r2 = qsr2 * cos( ( th + 4*pi ) / 3 ) - a03;

		if( r0 >= 0 && r0 <= 1 ) result.push( r0 );
		if( r1 >= 0 && r1 <= 1 ) result.push( r1 );
		if( r2 >= 0 && r2 <= 1 ) result.push( r2 );
	}
	return Object.freeze( result.sort( ) );
};

/*
| Zero tolerance.
*/
const epsilon =
def.static.epsilon =
	1e-9;

/*
| Returns true if v is within epsilon of zero.
*/
def.static.isZero = ( v ) => abs( v ) < epsilon;
