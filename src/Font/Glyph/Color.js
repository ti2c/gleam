/*
| A glyph with color.
*/
def.attributes =
{
	// the color
	color: { type: 'Color/Self' },

	// colorless glyph
	glyphBland: { type: 'Font/Glyph/Bland' },
};

import { Self as GlyphBland } from '{Font/Glyph/Bland}';
import { Self as Point      } from '{Figure/Point}';

/*
| Advance width.
*/
def.lazy.advanceWidth =
	function( )
{
	return this.glyphBland.advanceWidth;
};

/*
| The glyph index.
*/
def.lazy.index =
	function( )
{
	return this.glyphBland.index;
};

/*
| Font size of the glyph.
*/
def.lazy.fontSize =
	function( )
{
	return this.glyphBland.fontBland.size;
};

/*
| Creating shortcut.
*/
def.static.FontColorIndex =
	function( fontColor, index )
{
	return(
		Self.create(
			'color', fontColor.color,
			'glyphBland',
				GlyphBland.create(
					'fontBland', fontColor.fontBland,
					'index', index,
				)
		)
	);
};

/*
| Returns the kerning value to another glyph.
*/
def.proto.kerningValue =
	function( glyph )
{
	return this.glyphBland.kerningValue( glyph.glyphBland );
};


/*
| Creates a canvas with just the glyph for caching.
| Also sets _canvasPos for the position the glyph should take.
*/
def.lazy._canvas =
	function( )
{
	const glyphBland = this.glyphBland;
	const bb = glyphBland.boundingBoxInt;
	const pos = bb.pos;

	if( bb.width <= 0 || bb.height <= 0 )
	{
		// the glyph is empty.
		ti2c.aheadValue( this, '_canvasPos', true );
		return true;
	}

	ti2c.aheadValue( this, '_canvasPos', pos );

	const canvas = document.createElement( 'canvas' );
	canvas.width = bb.width || 1;
	canvas.height = bb.height || 1;

	const cx = canvas.getContext( '2d' );
	cx.translate( -pos.x, -pos.y );

	const path = glyphBland._otPath( Point.zero );
	path.fill = this.color.css;
	path.draw( cx );

	return canvas;
};

/*
| Offset position of the _canvas cache.
*/
def.lazy._canvasPos =
	function( )
{
	// Since _canvas aheads this, this function is only called
	// if it didn't do that yet. So do it, and the return the
	// aheaded result.
	this._canvas;
	return this._canvasPos;
};

/*
| Draws the glyph on a canvas.
|
| ~cx: canvas context to draw on.
| ~p: point to draw at.
*/
def.proto._drawOnCanvas =
	function( cx, p )
{
	const path = this.glyphBland._otPath( p );
	path.fill = this.color.css;
	path.draw( cx );
};

