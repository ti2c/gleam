/*
| A bland glyph (without color).
|
| FIXME there should be sizeless glyphs
*/
def.attributes =
{
	// colorless font family and size
	fontBland: { type: 'Font/Bland' },

	// the glpyh index
	index: { type: 'number' },
};

import { Self as Point } from '{Figure/Point}';
import { Self as Rect  } from '{Figure/Shape/Rect}';

/*
| Advance width.
*/
def.lazy.advanceWidth =
	function( )
{
	return this._otGlyph.advanceWidth * this.fontBland._fontScale;
};

/*
| Bounding box of the glyph.
| Floors/ceils to next integer.
*/
def.lazy.boundingBoxInt =
	function( )
{
	const path = this._otPath( Point.zero );
	const bb = path.getBoundingBox( );

	const x1 = Math.floor( bb.x1 );
	const y1 = Math.floor( bb.y1 );
	const x2 = Math.ceil( bb.x2 );
	const y2 = Math.ceil( bb.y2 );

	return(
		Rect.PosWidthHeight(
			Point.XY( x1, y1 ),
			x2 - x1,
			y2 - y1,
		)
	);
};

/*
| Creation shortcut.
*/
def.static.FontBlandIndex =
	function( fontBland, index )
{
	return(
		Self.create(
			'fontBland', fontBland,
			'index', index,
		)
	);
};

/*
| Font size of the glyph.
*/
def.lazy.fontSize =
	function( )
{
	return this.fontBland.size;
};

/*
| Returns the kerning value to another glyph.
*/
def.lazyFunc.kerningValue =
	function( glyph )
{
	return this.fontBland.family._opentype.getKerningValue( this._otGlyph, glyph._otGlyph );
};

/*
| Opentype implementation.
*/
def.lazy._otGlyph =
	function( )
{
	return this.fontBland.family._opentype.glyphs.get( this.index );
};

/*
| The opentype path.
|
| ~p: base point
*/
def.proto._otPath =
	function( p )
{
	const fontBland = this.fontBland;

	return(
		this._otGlyph.getPath(
			p.x, p.y,
			// rounding here is argueable, font hinting sometimes acts wierd
			// on fraction pt values
			Math.round( this.fontSize ),
			fontBland._options,
			fontBland.family._opentype
		)
	);
};
