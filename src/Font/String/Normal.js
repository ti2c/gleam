/*
| A string in a font with a specific size and color.
*/
def.attributes =
{
	// face of the font
	fontColor: { type: 'Font/Color' },

	// the bland string to draw
	stringBland: { type: 'Font/String/Bland' },
};

/*
| Glyphs for fonts larger than this won't be cached.
*/
const glyphCacheLimit = 250;

import { Self as FontStringBland } from '{Font/String/Bland}';
import { Self as GlyphColor      } from '{Font/Glyph/Color}';
import { Self as Point           } from '{Figure/Point}';

/*
| Measures the advance width.
*/
def.lazy.advanceWidth =
	function( )
{
	return this.stringBland.advanceWidth;
};

/*
| Create Shortcut.
|
| ~fontColor: the colored font to use.
| ~string: the string
*/
def.static.FontColorString =
	function( fontColor, string )
{
	return(
		Self.create(
			'fontColor', fontColor,
			'stringBland', FontStringBland.FontBlandString( fontColor.fontBland, string ),
		)
	);
};

/*
| Returns the width of the token.
*/
def.lazy.width =
	function( )
{
	return this.stringBland.width;
};

/*
| Draws the string.
|
| ~point:      point
| ~align:      horizontal align
| ~base:       vertial align
| ~resolution: resolution to use for hinting
| ~cx:         canvas context to draw it on.
*/
def.proto._drawOnCanvas =
	function( point, align, base, resolution, cx )
{
/**/if( CHECK && arguments.length !== 5 ) throw new Error( );

	const fontColor = this.fontColor;
	const fontBland = fontColor.fontBland;
	const size = fontBland.size;

	const fontScale = fontBland._fontScale;
	const glyphs = this._glyphs;

	const x0 = point.x;
	const y0 = point.y;
	let x = 0;
	let y = 0;

	switch( align )
	{
		case 'center':
			x -= this.width / 2;
			break;

		case 'left':
			break;

		case 'right':
			x -= this.width;
			break;

		default:
			throw new Error( );
	}

	switch( base )
	{
		case 'alphabetic':
			break;

		case 'middle':
			y += fontBland.family.capHeight * fontScale / 2 - 0.5;
			break;

		default:
			throw new Error( );
	}

	let glyph;
	for( let a = 0, alen = glyphs.length; a < alen; a++ )
	{
		glyph = glyphs[ a ];

		if( size >= glyphCacheLimit )
		{
			const xx = x + x0;
			const yy = y + y0;
			const pp = Point.XY( resolution.fit( xx ), resolution.fit( yy ) );
			glyph._drawOnCanvas( cx, pp );
		}
		else
		{
			const gCanvas = glyph._canvas;
			const gCPos = glyph._canvasPos;
			if( gCanvas !== true )
			{
				const xx = x + gCPos.x;
				const yy = y + gCPos.y;
				cx.drawImage(
					gCanvas,
					resolution.fit( xx + x0 ),
					resolution.fit( yy + y0 )
				);
			}
		}

		if( a + 1 < alen )
		{
			const aw = glyph.advanceWidth;
			if( aw ) x += aw;
			x += glyph.kerningValue( glyphs[ a + 1 ] ) * fontScale;
		}
	}
};

/*
| The glyphs.
*/
def.lazy._glyphs =
	function( )
{
	const fontColor = this.fontColor;
	const color = fontColor.color;

	let bGlyphs = this.stringBland._glyphs;
	let glyphs = [ ];

	for( let bGlyph of bGlyphs )
	{
		glyphs.push(
			GlyphColor.create( 'color', color, 'glyphBland', bGlyph )
		);
	}
	return glyphs;
};
