/*
| Manages all fonts (on server and shell)
*/
def.abstract = true;

import { Self as FontFamily      } from '{Font/Family}';
import { Self as GroupFontFamily } from '{group@Font/Family}';

/*
| The cache pool
*/
let pool;

/*
| Factor to add to the bottom of font height.
| TODO remove
*/
def.static.bottomBox = 0.35;


if( NODE )
{
	//FIXME, currently not operational in NODE

	//const opentype = await import( 'opentype.js' ).default;

	/*
	| Loads a font (node version)
	|
	| ~name:     the name of the font to load
	| ~callback: the callback when done so
	*/
	/*
	def.static.load =
		async function( name )
	{
		return(
			new Promise(
				( resolve, reject ) =>
			{
				opentype.load(
					'./font/' + name + '.ttf',
					( err, otFont ) =>
				{
					if( err )
					{
						reject(
							'Font "' + name + '" could not be loaded: ' + err
						);
						return;
					}

					const fontFamily =
						FontFamily.create(
							'name', name,
							'_opentype', otFont
						);
					pool = ( pool || Self ).create( 'group:set', name, fontFamily );
					resolve( fontFamily );
				} );
			} )
		);
	};
	*/
}
else
{
	/*
	| Loads a font (browser version)
	|
	| ~name:     the name of the font to load
	*/
	def.static.load =
		function( name )
	{
		return(
			new Promise(
				( resolve, reject ) =>
			{
				opentype.load(
					//'/font-' + name + '.ttf',
					'/font-' + name + '.woff',
					( err, otFont ) =>
				{
					if( err )
					{
						reject(
							'Font "' + name + '" could not be loaded: ' + err
						);
						return;
					}

					const fontFamily =
						FontFamily.create(
							'name', name,
							'_opentype', otFont
						);

					pool =
						( pool || GroupFontFamily )
						.create( 'group:set', name, fontFamily );

					resolve( fontFamily );
				} );
			} )
		);
	};

	/*
	| Loads a font from base64 data.
	|
	| ~name:     the name of the font to load
	*/
	def.static.loadB64 =
		function( name, b64data )
	{
		return(
			new Promise(
				( resolve, reject ) =>
			{
				opentype.load(
					'data:font/ttf;base64,' + b64data,
					( err, otFont ) =>
				{
					if( err )
					{
						reject(
							'Font "' + name + '" could not be loaded: ' + err
						);
						return;
					}

					const fontFamily =
						FontFamily.create(
							'name', name,
							'_opentype', otFont
						);

					pool =
						( pool || GroupFontFamily )
						.create( 'group:set', name, fontFamily );

					resolve( fontFamily );
				} );
			} )
		);
	};
}

/*
| Gets a font
*/
def.static.get =
	( name ) =>
	pool.get( name );
