/*
| Initializes the ti2c-gleam package.
*/
const ti2c = ( await import( 'ti2c') ).default;
await ti2c.register(
	'name',    'gleam',
	'meta',    import.meta,
	'source',  'src/',
	'relPath', 'init'
);
