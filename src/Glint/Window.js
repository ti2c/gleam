/*
| Puts a pane on display.
*/
def.attributes =
{
	// name of the clint for debugging & testsuite
	name: { type: [ 'undefined', 'string' ], json: true },

	// the pane(cutout) to display
	pane: { type: 'Glint/Pane', json: true },

	// position of the window
	pos: { type: 'Figure/Point', json: true }
};

def.json = true;

/*
| Shortcut.
*/
def.static.PanePos =
	( pane, pos ) =>
	Self.create( 'pane', pane, 'pos', pos );
