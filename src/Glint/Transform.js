/*
| Transforms all sublints
*/
def.attributes =
{
	// the glints to transform
	glint: { type: [ '< Glint/Types' ], json: true },

	// name of the clint for debugging & testsuite
	name: { type: [ 'undefined', 'string' ], json: true },

	// the transform to apply
	transform: { type: [ '< Transform/Types' ], json: true },
};

def.json = true;
