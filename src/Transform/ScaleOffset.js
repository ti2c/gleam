/*
| A coordinate transformation.
*/
def.extend = 'Transform/Base';

def.attributes =
{
	// coordinate offset
	offset: { type: 'Figure/Point' },

	// the scale factor
	scale: { type: 'number' },
};

def.json = true;
def.create = [ '_create' ];

import { Self as Normal            } from '{Transform/Normal}';
import { Self as Point             } from '{Figure/Point}';
import { Self as RotateScaleOffset } from '{Transform/RotateScaleOffset}';
import { Self as Scale             } from '{Transform/Scale}';

/*
| Adds an offset to the transfrom.
| This addition is affected by the scale.
*/
def.proto.addOffset =
	function( p )
{
	const s = this.scale;
	return this._create( 'offset', this.offset.add( p.x * s, p.y * s ) );
};

/*
| Adds rotation to the transfrom
*/
def.proto.addRotate =
def.proto.setRotate =
	function( rotation )
{
	return(
		RotateScaleOffset._create(
			'offset',   this.offset,
			'rotation', rotation,
			'scale',    this.scale
		)
	);
};

/*
| Returns a transform which does the same
| as the combination of this and t.
|
| ~t: transform to combine.
*/
def.proto.combine =
	function( t )
{
	if( t.ti2ctype === Normal )
	{
		return this;
	}

	if( t.ti2ctype !== Self ) throw new Error( );

	const offset = this.offset;
	const scale = this.scale;
	const tOffset = t.offset;
	return(
		Self._create(
			'offset',
				Point.XY(
					offset.x + tOffset.x * scale,
					offset.y + tOffset.y * scale
				),
			'scale', scale * t.scale
		)
	);
};

/*
| Returns a transformed distance.
*/
def.proto.d =
	function( d )
{
	return this.scale * d;
};

/*
| Returns a detransformed distance.
*/
def.proto.ded =
	function( d )
{
	return d / this.scale;
};

/*
| Returns a reverse transformed x value.
*/
def.proto.dex =
	function( x )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( x ) !== 'number' || arguments.length !== 1 ) throw new Error( );
/**/}

	return ( x  - this.offset.x ) / this.scale;
};

/*
| Returns the reverse transformed y value.
*/
def.proto.dey =
	function( y )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( y ) !== 'number' || arguments.length !== 1 ) throw new Error( );
/**/}
	return ( y - this.offset.y ) / this.scale;
};

/*
| Changes the scale of this transform.
*/
def.proto.scaleBy =
	function( scale )
{
	return this._create( 'scale', this.scale * scale );
};

/*
| Returns a transform with the same scale as this, but with a zero offset.
*/
def.lazy.scaleOnly =
	function( )
{
	return Scale._create( 'scale', this.scale );
};

/*
| Changes the scale of this transform.
*/
def.proto.setOffset =
	function( offset )
{
	return this._create( 'offset', offset );
};

/*
| Changes the scale of this transform.
*/
def.proto.setScale =
	function( scale )
{
	return this._create( 'scale', scale );
};

/*
| Changes the scale and offset of this transform.
*/
def.proto.setScaleOffset =
	function( scale, offset )
{
	return this._create( 'offset', offset, 'scale', scale );
};

/*
| Returns a transformed x value.
*/
def.proto.x =
	function( x )
{
/**/if( CHECK && ( typeof( x ) !== 'number' || arguments.length !== 1 ) ) throw new Error( );

	return x * this.scale + this.offset.x;
};

/*
| Returns a transformed y value.
*/
def.proto.y =
	function( y )
{
/**/if( CHECK && ( typeof( y ) !== 'number' || arguments.length !== 1 ) ) throw new Error( );

	return y * this.scale + this.offset.y;
};
