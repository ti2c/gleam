/*
| A coordinate transformation.
*/
def.extend = 'Transform/Base';

def.attributes =
{
	// the scale factor
	scale: { type: 'number' },
};

def.json = true;
def.create = [ '_create' ];


import { Self as Point             } from '{Figure/Point}';
import { Self as Normal            } from '{Transform/Normal}';
import { Self as ScaleOffset       } from '{Transform/ScaleOffset}';
import { Self as RotateScaleOffset } from '{Transform/RotateScaleOffset}';

/*
| Adds an offset to the transfrom.
| This addition is affected by the scale.
*/
def.proto.addOffset =
	function( p )
{
	const s = this.scale;
	return(
		ScaleOffset._create(
			'offset', Point.XY( p.x * s, p.y * s ),
			'scale',  s
		)
	);
};

/*
| Adds rotation to the transfrom
*/
def.proto.addRotate =
def.proto.setRotate =
	function( rotation )
{
	return(
		RotateScaleOffset._create(
			'rotation', rotation,
			'scale', this.scale,
			'offset', Point.zero,
		)
	);
};

/*
| Returns a transform which does the same
| as the combination of this and t.
|
| ~t: transform to combine.
*/
def.proto.combine =
	function( t )
{
	switch( t.ti2ctype )
	{
		case Normal: return this;
		case Self: return Self._create( 'scale', this.scale * t.scale );
		case ScaleOffset:
		{
			const tOffset = t.offset;
			const scale = this.scale;
			return(
				ScaleOffset._create(
					'scale', t.scale * scale,
					'offset',
						Point.XY(
							tOffset.x * scale,
							tOffset.y * scale
						)
				)
			);
		}
		default: throw new Error( );
	}
};

/*
| Returns a transformed distance.
*/
def.proto.d =
	function( d )
{
	return this.scale * d;
};

/*
| Returns a detransformed distance.
*/
def.proto.ded =
	function( d )
{
	return d / this.scale;
};

/*
| Returns a reverse transformed x value.
*/
def.proto.dex =
	function( x )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( x ) !== 'number' || arguments.length !== 1 ) throw new Error( );
/**/}
	return x  / this.scale;
};

/*
| Returns the reverse transformed y value.
*/
def.proto.dey =
	function( y )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( y ) !== 'number' || arguments.length !== 1 ) throw new Error( );
/**/}
	return y / this.scale;
};

/*
| Shortcut.
*/
def.static.OffsetScale =
	( offset, scale ) =>
	Self.create( 'offset', offset, 'scale', scale );

/*
| Adds rotation to the transfrom
*/
def.proto.setRotation =
	function( rotation )
{
	return(
		RotateScaleOffset.create(
			'offset', Point.zero,
			'rotation', rotation,
			'scale', this.scale
		)
	);
};

/*
| Changes the scale of this transform.
*/
def.proto.scaleBy =
	function( scale )
{
	return this.create( 'scale', this.scale * scale );
};

/*
| Returns a transform with the same scale as this, but with a zero offset.
*/
def.lazy.scaleOnly =
	function( )
{
	return this;
};

/*
| Changes the offset of this transform.
*/
def.proto.setOffset =
	function( offset )
{
	return ScaleOffset._create( 'offset', offset, 'scale', this.scale );
};

/*
| Returns a transformed x value.
*/
def.proto.x =
	function( x )
{
/**/if( CHECK && ( typeof( x ) !== 'number' || arguments.length !== 1 ) ) throw new Error( );

	return x * this.scale;
};

/*
| Returns a transformed y value.
*/
def.proto.y =
	function( y )
{
/**/if( CHECK && ( typeof( y ) !== 'number' || arguments.length !== 1 ) ) throw new Error( );

	return y * this.scale;
};
