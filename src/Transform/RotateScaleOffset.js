/*
| A coordinate transformation
| Including rotation.
*/
def.extend = 'Transform/Base';

def.attributes =
{
	// coordinate offset
	offset: { type: 'Figure/Point' },

	// rotation
	rotation: { type: [ '< Figure/Angle/Types' ] },

	// the scale factor
	scale: { type: 'number' },
};

def.json = true;
def.create = [ '_create' ];

/*
| Returns a transformed distance.
*/
def.proto.d =
	function( d )
{
	return this.scale * d;
};

/*
| Returns a detransformed distance.
*/
def.proto.ded =
	function( d )
{
	return d / this.scale;
};

/*
| Shortcut with caching.
*/
def.lazyFunc.scale =
	function( scale )
{
	return this.create( 'scale', scale );
};

/*
| Returns a transformed x value.
*/
def.proto.x =
	function( x, y )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( x ) !== 'number' ) throw new Error( );
/**/	if( typeof( y ) !== 'number' ) throw new Error( );
/**/}

	const scale = this.scale;
	const rot = this.rotation;
	const xs = x * scale;
	const ys = y * scale;

	return xs * rot.cos + ys * rot.sin + this.offset.x;
};

/*
| Returns a transformed y value.
*/
def.proto.y =
	function( x, y )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( x ) !== 'number' ) throw new Error( );
/**/	if( typeof( y ) !== 'number' ) throw new Error( );
/**/}

	const scale = this.scale;
	const rot = this.rotation;
	const xs = x * scale;
	const ys = y * scale;

	return ys * rot.cos - xs * rot.sin + this.offset.y;
};
