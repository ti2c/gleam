/*
| Base of all transforms.
*/
def.abstract = true;

import { Self as Normal            } from '{Transform/Normal}';
import { Self as Scale             } from '{Transform/Scale}';
import { Self as ScaleOffset       } from '{Transform/ScaleOffset}';
import { Self as RotateScaleOffset } from '{Transform/RotateScaleOffset}';

/*
| Returns true if 't' is a transform.
*/
def.static.isa =
	function( t )
{
	return(
		t.ti2ctype === Normal
		|| t.ti2ctype === RotateScaleOffset
		|| t.ti2ctype === Scale
		|| t.ti2ctype === ScaleOffset
	);
};
