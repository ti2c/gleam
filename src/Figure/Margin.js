/*
| Holds information of inner or outer distances.
*/
def.attributes =
{
	// north
	n: { type: 'number' },

	// east
	e: { type: 'number' },

	// south
	s: { type: 'number' },

	// west
	w: { type: 'number' },
};

/*
| Shortcut.
*/
def.static.NESW =
	( n, e, s, w ) =>
	Self.create( 'n', n, 'e', e, 's', s, 'w', w );

/*
| east + west margin = x margin
*/
def.lazy.x =
	function( )
{
	return this.e + this.w;
};

/*
| north + south margin = y margin
*/
def.lazy.y =
	function( )
{
	return this.n + this.s;
};
