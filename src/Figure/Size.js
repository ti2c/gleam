/*
| A size.
|
| Positionless dimensions.
*/
def.attributes =
{
	height: { type: 'number', json: true },

	width: { type: 'number', json: true },
};

def.json = true;

import { Self as Point         } from '{Figure/Point}';
import { Self as Rect          } from '{Figure/Shape/Rect}';
import { Self as TransformBase } from '{Transform/Base}';

const ceil  = Math.ceil;
const trunc = Math.trunc;

/*
| Returns the size increased by
|
| add( size )    -or-
| add( point )   -or-
| add( w, h  )
*/
def.proto.add =
	function( a1, a2 )
{
	if( typeof( a1 ) === 'number' )
	{
		if( a2 === undefined )
		{
			a2 = a1;
		}
		return Self.WH( this.width + a1, this.height + a2 );
	}
	else if( a1.ti2ctype === Self )
	{
		return Self.WH( this.width + a1.width, this.height + a2.height );
	}
	else if( a1.ti2ctype === Point )
	{
		return Self.WH( this.width + a1.x, this.height + a2.x );
	}
	else
	{
		throw new Error( );
	}
};

/*
| Descales this by s.
*/
def.proto.descale =
	function( s )
{
/**/if( CHECK && typeof( s ) !== 'number' ) throw new Error( );

	return Self.WH( this.width / s, this.height / s );
};

/*
| Returns this detransformed rect.
*/
def.proto.detransform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return this.scale( 1 / transform.scale );
};

/*
| An infinite size.
*/
def.staticLazy.Infinity =
	( ) =>
	Self.create(
		'height', Number.POSITIVE_INFINITY,
		'width',  Number.POSITIVE_INFINITY,
	);

/*
| Creates the size of current window.
*/
def.static.InnerWindow =
	( ) =>
	Self.create(
		'height', window.innerHeight,
		'width',  window.innerWidth,
	);

/*
| Scales this by s.
*/
def.proto.scale =
	function( s )
{
/**/if( CHECK && typeof( s ) !== 'number' ) throw new Error( );

	return Self.WH( this.width * s, this.height * s );
};

/*
| Scales and ceilings this by s.
*/
def.proto.scaleCeil =
	function( s )
{
/**/if( CHECK && typeof( s ) !== 'number' ) throw new Error( );

	return Self.WH( ceil( this.width * s ), ceil( this.height * s ) );
};

/*
| Scales and truncates this by s.
*/
def.proto.scaleTrunc =
	function( s )
{
/**/if( CHECK && typeof( s ) !== 'number' ) throw new Error( );

	return Self.WH( trunc( this.width * s ), trunc( this.height * s ) );
};

/*
| Transforms this.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	return this.scale( transform.scale );
};

/*
| Shortcut.
*/
def.static.WH =
	( width, height ) =>
	Self.create( 'width', width, 'height', height );

/*
| A rectangle of same size with p at 0/0.
*/
def.lazy.zeroRect =
	function( )
{
	return Rect.PosWidthHeight( Point.zero, this.width, this.height );
};
