/*
| A generic angle, everything but 0°, 45°, 90°, 135°, 180°, 225°, 270°, 315°
*/
def.extend = 'Figure/Angle/Base';

def.attributes =
{
	// value in radians
	radians: { type: 'number', json: true },
};

def.json = true;

import { Self as Angle } from '{Figure/Angle/Self}';
import { Self as Point } from '{Figure/Point}';

const atan2 = Math.atan2;
const cos = Math.cos;
const sin = Math.sin;
const pi = Math.PI;
const pi2 = 2 * pi;
const pi025 = pi / 4;
const pi075 = 3 * pi025;
const pi125 = 5 * pi025;
const pi175 = 7 * pi025;

/*
| Returns an angle one eigth clock wise.
| FIXME rename cw8
*/
def.lazy.cw08 =
	function( )
{
	const r = this.create( 'radians', Angle.restrict( this.radians - pi025 ) );
	ti2c.aheadValue( r, 'ccw08', this );
	return r;
};

/*
| Returns an angle one eigth counter clock wise.
| FIXME rename ccw8
*/
def.lazy.ccw08 =
	function( )
{
	return this.create( 'radians', Angle.restrict( this.radians + pi025 ) );
};

/*
| Cosinus valus of this angle.
*/
def.lazy.cos =
	function( )
{
	return cos( this.radians );
};

/*
| Creates a generic angle between two points
| ~po: origin
| ~p0: point 0
| ~p1: point 1
*/
def.static.createBetweenPoints =
	function( po, p0, p1 )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( po.ti2ctype !== Point ) throw new Error( );
/**/	if( p0.ti2ctype !== Point ) throw new Error( );
/**/	if( p1.ti2ctype !== Point ) throw new Error( );
/**/}

	let r = atan2( p1.y - po.y, p1.x - po.x ) - atan2( p0.y - po.y, p0.x - po.x );

	if( r < 0 )
	{
		r = r + 2 * pi;
	}

	return Self.create( 'radians', r );
};

/*
| Creates a generic angle (relative to zero) that is in the middle
| of two points.
|
| ~po: origin
| ~p0: point 1
| ~p1: point 2
*/
def.static.createMiddleOfPoints =
	function( po, p0, p1 )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( po.ti2ctype !== Point ) throw new Error( );
/**/	if( p0.ti2ctype !== Point ) throw new Error( );
/**/	if( p1.ti2ctype !== Point ) throw new Error( );
/**/}

	const r1 = atan2( po.y - p0.y, p0.x - po.x );
	const r2 = atan2( po.y - p1.y, p1.x - po.x );
	let r = ( r1 + r2 ) / 2;

	if( r < 0 )
	{
		r = r + 2 * pi;
	}

	return Self.create( 'radians', r );
};

/*
| Converts the angle into degree.
*/
def.lazy.degree =
	function( )
{
	return this.radians * 180 / pi;
};

/*
| Has y component.
*/
def.proto.hasX = true;
def.proto.hasY = true;

/*
| Has n component.
*/
def.lazy.hasN =
	function( )
{
	const r = this.radians;

	return r > 0 && r < pi;
};

/*
| Returns the nearest (intermediate) cardinal direction.
*/
def.lazy.nearestIDir =
	function( )
{
	let d = Math.round( this.radians * 4 / pi );
	return Angle.iDirByIndex( d );
};

/*
| Opposite direction.
*/
def.lazy.opposite =
	function( )
{
	return this.create( 'radians', ( this.radians + pi ) % pi2 );
};

/*
| Sinus valus of this angle.
*/
def.lazy.sin =
	function( )
{
	return sin( this.radians );
};

/*
| True if the angle is nearer to the y-axis than the x-axis.
*/
def.lazy.steep =
	function( )
{
	const r = this.radians;

	if( r <= pi025 || r >= pi175 )
	{
		return false;
	}

	if( r < pi075 || r > pi125 )
	{
		return true;
	}

	return false;
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	const r = this.radians;
/**/	if( r < 0 || r > pi2 ) throw new Error( );
/**/}
};
