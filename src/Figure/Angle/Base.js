/*
| Abstract parent of angles.
*/
def.abstract = true;

import { Self as Angle } from '{Figure/Angle/Self}';

const pi = Math.PI;
const pi2 = 2 * pi;

/*
| Returns the angle between two angles.
*/
def.proto.between =
	function( angle )
{
	let r = ( this.radians + angle.radians ) / 2;

	if( r > pi )
	{
		r -= pi2;
	}

	if( r < -pi )
	{
		r += pi2;
	}

	return Angle.Radians( r );
};

/*
| Radians between two angles (from -pi to pi)
*/
def.proto.radiansBetween =
	function( angle )
{
	let r = this.radians - angle.radians;

	if( r > pi )
	{
		r -= pi2;
	}

	if( r < -pi )
	{
		r += pi2;
	}

	return r;
};
