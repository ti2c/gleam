/*
| South-east, 315°
*/
def.extend = 'Figure/Angle/Base';
def.singleton = true;

def.json = true;

import { Self as Angle } from '{Figure/Angle/Self}';

/*
| One intermediate cardinal step counter clockwise.
*/
def.lazy.ccw08 = ( ) => Angle.e;

/*
| Cosinus value of this angle.
*/
def.proto.cos = 0.7071067811865476;

/*
| One intermediate cardinal step clockwise.
*/
def.lazy.cw08 = ( ) => Angle.s;

/*
| Gets point from a rect.
*/
def.proto.from = ( rect ) => rect.pse;

/*
| Has x component.
*/
def.proto.hasX = true;

/*
| Has y component.
*/
def.proto.hasY = true;

/*
| Opposite direction.
*/
def.lazy.opposite = ( ) => Angle.nw;

/*
| Radians constant.
*/
def.proto.radians = Math.PI * 7 / 4;

/*
| Sinus value of this angle.
*/
def.proto.sin = -0.7071067811865476;

/*
| True if the angle is nearer to the y-axis than the x-axis.
*/
def.proto.steep = false;
