/*
| North-west, 135°
*/
def.extend = 'Figure/Angle/Base';
def.singleton = true;

def.json = true;

import { Self as Angle } from '{Figure/Angle/Self}';

/*
| One intermediate cardinal step counter clockwise.
*/
def.lazy.ccw08 = ( ) => Angle.w;

/*
| Cosinus value of this angle.
*/
def.proto.cos = -0.7071067811865476;

/*
| One intermediate cardinal step clockwise.
*/
def.lazy.cw08 = ( ) => Angle.n;

/*
| Gets point from a rect.
*/
def.proto.from = ( rect ) => rect.pos;

/*
| Has x component.
*/
def.proto.hasX = true;

/*
| Has y component (s or n)
*/
def.proto.hasY = true;

/*
| Has n component.
*/
def.proto.hasN = true;

/*
| Opposite direction.
*/
def.lazy.opposite = ( ) => Angle.se;

/*
| Radians constant.
*/
def.proto.radians = Math.PI * 3 / 4;

/*
| Sinus value of this angle.
*/
def.proto.sin = 0.7071067811865475;

/*
| True if the angle is nearer to the y-axis than the x-axis.
*/
def.proto.steep = false;
