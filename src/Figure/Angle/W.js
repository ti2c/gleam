/*
| West, 180°
*/
def.extend = 'Figure/Angle/Base';
def.singleton = true;

def.json = true;

import { Self as Angle } from '{Figure/Angle/Self}';

/*
| One intermediate cardinal step counter clockwise.
*/
def.lazy.ccw08 = ( ) => Angle.sw;

/*
| Cosinus value of this angle.
*/
def.proto.cos = -1;

/*
| One intermediate cardinal step clockwise.
*/
def.lazy.cw08 = ( ) => Angle.nw;

/*
| Gets point from a rect.
*/
def.proto.from = ( rect ) => rect.pw;

/*
| Has x component.
*/
def.proto.hasX = true;

/*
| Has no y component.
*/
def.proto.hasY = false;

/*
| Opposite direction.
*/
def.lazy.opposite = ( ) => Angle.e;

/*
| Radians constant.
*/
def.proto.radians = Math.PI;

/*
| Sinus value of this angle.
*/
def.proto.sin = 0;

/*
| True if the angle is nearer to the y-axis than the x-axis.
*/
def.proto.steep = false;
