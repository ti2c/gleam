/*
| A quatratic curve.
*/
def.attributes =
{
	// start point
	p0: { type: 'Figure/Point', json: true },

	// end point
	p1: { type: 'Figure/Point', json: true },

	// control point
	pco: { type: 'Figure/Point', json: true },
};

def.json = true;

import { Self as Rect    } from '{Figure/Shape/Rect}';
import { Self as Segment } from '{Figure/Path/Segment}';
import { Self as Path    } from '{Figure/Path/Self}';
import { Self as Point   } from '{Figure/Point}';

const atan2 = Math.atan2;
const cos = Math.cos;
const max = Math.max;
const min = Math.min;
const pi = Math.PI;
const pi05 = pi / 2;
const sin = Math.sin;
const sqrt = Math.sqrt;

/*
| Moves the qcurve by p or x/y.
*/
def.proto.add =
	function( /* p or x,y */ )
{
	const p0 = this.p0;
	const pco = this.pco;
	const p1 = this.p1;

	return(
		this.create(
			'p0', p0.add.apply( p0, arguments ),
			'p1', p1.add.apply( p1, arguments ),
			'pco', pco.add.apply( pco, arguments )
		)
	);
};

/*
| Apportions the qcurve into non-sharp parts.
*/
def.lazy.apportion =
	function( )
{
	/*
	const list = [ this ];
	let i = 0;
	while( i < list.length )
	{
		const qc = list[ i ];
		const dphi = abs( abs( qc.phi0co - qc.phi1co ) - pi );
		if( dphi < pi033 ) i++;
		else qc._split( list, 0.5, i, 1 );
	}
	return QCurveList.create( 'list:init', list );
	*/
	throw new Error( );
};

/*
| Makes a primitive offset approximation without splitting.
*/
def.proto.approxOffset =
	function( d )
{
	const p0 = this.p0;
	const p1 = this.p1;

	const phi0 = this.phi0co + pi05;
	const phi1 = this.phi1co - pi05;

	const p0of = Point.XY( p0.x + cos( phi0 ) * d, p0.y + sin( phi0 ) * d );
	const p1of = Point.XY( p1.x + cos( phi1 ) * d, p1.y + sin( phi1 ) * d );

	return this.scaleTo( p0of, p1of );
};

/*
| Returns something with distance d next to this qcurve.
*/
def.lazyFunc.envelope =
	function( d )
{
	throw new Error( );
};

/*
| Finds the intersections of the quadratic with something else.
|
| Returns an array with doublet at values.
|
| ~figure:      the figure to find intersections with
| ~extended:    if defined figures are to be extended beyond their end.
| ~flip:        if defined flips results
| ~resultArray: if defined appends intersections to this array
| ~isLeft:      true if figure is left of this
| ~isRight:     true if figure is right of this
*/
def.proto.intersectsAt =
	function( figure, extended, flip, resultArray, isLeft, isRight )
{
	const zone = this.zone;
	const fzone = figure.zone;

	if( !zone.overlaps( fzone ) ) return resultArray;

	switch( figure.ti2ctype )
	{
		case Path:
			return figure.intersectsAt( this, extended, !flip, resultArray );

		case Segment:
			return(
				this._intersectsSegmentAt(
					figure, extended, flip, resultArray, isLeft, isRight
				)
			);

		case Self:
			throw new Error( );

		default:
			throw new Error( );
	}
};

/*
| Get the first intersection of the qcurve with another shape.
*/
def.proto.intersectsPoint =
	function( shape )
{
	throw new Error( );
};

/*
| Returns the intersection of the qcurve with a line segment.
|
| ~segment:     the figure to find intersections with
| ~extended:    if defined figures are to be extended beyond their end.
| ~flip:        if defined flips results
| ~resultArray: if defined push to this
| ~isLeft:      true if figure is left of this
| ~isRight:     true if figure is right of this
*/
def.proto._intersectsSegmentAt =
	function( segment, extended, flip, resultArray, isLeft, isRight )
{
/**/if( CHECK )
/**/{
/**/	if( segment.ti2ctype !== Segment ) throw new Error( );
/**/}

	const lp0 = segment.p0;
	const lp1 = segment.p1;

	const lp0x = lp0.x;
	const lp0y = lp0.y;
	const lp1x = lp1.x;
	const lp1y = lp1.y;

	const p0  = this.p0;
	const pco = this.pco;
	const p1  = this.p1;

	const p0x = p0.x;
	const p0y = p0.y;
	const pcox = pco.x;
	const pcoy = pco.y;
	const p1x = p1.x;
	const p1y = p1.y;

	let a, b, c, k;
	if( lp0x === lp1x )
	{
		a = p0x - 2*pcox + p1x;
		b = -2*( p0x - pcox );
		c = p0x - lp0x;
	}
	else if( lp0y === lp1y )
	{
		a = p0y - 2*pcoy + p1y;
		b = -2*( p0y - pcoy );
		c = p0y - lp0y;
	}
	else
	{
		k = (lp0y - lp1y ) / (lp1x - lp0x );
		a = k * (p0x - 2*pcox + p1x) + p0y - 2*pcoy + p1y;
		b = -2 * ( k * ( p0x - pcox ) + p0y - pcoy );
		c = k * (p0x - lp0x) + p0y - lp0y;
	}

	if( a === 0 )
	{
		const t = -c/b;

		if( t < 0 || t > 1 )
		{
			return resultArray;
		}

		const p = this.pointAt( t );
		const s =
			lp0x !== lp1x  // not a vertical line
			? ( ( p.x - lp0.x ) / ( lp1.x - lp0.x ) )
			: ( ( p.y - lp0.y ) / ( lp1.y - lp0.y ) );

		if( s < 0 || s > 1 )
		{
			return resultArray;
		}

		if( !resultArray ) resultArray = [ ];

		if( !flip )
		{
			resultArray.push( t, s );
		}
		else
		{
			resultArray.push( s, t );
		}

		return resultArray;
	}

	const d = b * b - 4 * a * c;
	if( d < 0 ) return resultArray;

	{
		const t = (-b - sqrt(d)) / (2 * a);
		if( t >= 0 && t <= 1 )
		{
			const p = this.pointAt( t );
			const s =
				lp0x !== lp1x  // not a vertical line
				? ( ( p.x - lp0.x ) / ( lp1.x - lp0.x ) )
				: ( ( p.y - lp0.y ) / ( lp1.y - lp0.y ) );

			if( !resultArray ) resultArray = [ ];

			if( s >= 0 && s <= 1 )
			{
				if( !flip )
				{
					resultArray.push( t, s );
				}
				else
				{
					resultArray.push( s, t );
				}
			}
		}
	}

	{
		const t = (-b + sqrt(d)) / (2 * a);
		if( t >= 0 && t <= 1 )
		{
			const p = this.pointAt( t );
			const s =
				lp0x !== lp1x  // not a vertical line
				? ( ( p.x - lp0.x ) / ( lp1.x - lp0.x ) )
				: ( ( p.y - lp0.y ) / ( lp1.y - lp0.y ) );

			if( !resultArray ) resultArray = [ ];

			if( s >= 0 && s <= 1 )
			{
				if( !flip )
				{
					resultArray.push( t, s );
				}
				else
				{
					resultArray.push( s, t );
				}
			}
		}
	}

	return resultArray;
};

/*
| Creation shortcut.
*/
def.static.P0PcoP1 =
	function( p0, pco, p1 )
{
	return Self.create( 'p0', p0, 'pco', pco, 'p1', p1 );
};

/*
| Creation shortcut.
|
| Creates using a "middle" point instead
| of control point.
*/
def.static.P0PmP1 =
	function( p0, pm, p1 )
{
	const d0 = pm.distanceOfPoint( p0 );
	const d1 = pm.distanceOfPoint( p1 );
	const t = d0 / ( d0 + d1 );
	const rt = 1 - t;

	const pco =
		Point.XY(
			( pm.x - rt * rt * p0.x - t * t * p1.x ) / ( 2 * t * rt ),
			( pm.y - rt * rt * p0.y - t * t * p1.y ) / ( 2 * t * rt )
		);

	/*
	const pco =
		Point.XY(
			2 * pm.x - p0.x / 2 - p1.x / 2,
			2 * pm.y - p0.y / 2 - p1.y / 2
		);
	*/

	return Self.create( 'p0', p0, 'pco', pco, 'p1', p1 );
};

/*
| Radiants of angle p0 to pc0.
*/
def.lazy.phi0co =
	function( )
{
	const p0 = this.p0;
	const pco = this.pco;
	return atan2( p0.y - pco.y, p0.x - pco.x );
};

/*
| Radiants of angle p1 to pc0.
*/
def.lazy.phi1co =
	function( )
{
	const p1 = this.p1;
	const pco = this.pco;
	return atan2( p1.y - pco.y, p1.x - pco.x );
};

/*
| Returns point at t (from 0 to 1 ).
*/
def.lazyFunc.pointAt =
	function( t )
{
/**/if( CHECK && !( t >= 0 && t <= 1 ) ) throw new Error( );

	const s = 1-t;

	const p0 = this.p0;
	const pco = this.pco;
	const p1 = this.p1;

	// FIXME use coeffs
	//const c = this._coeffs;

	return(
		Point.XY(
			s*s*p0.x + 2*s*t*pco.x + t*t*p1.x,
			s*s*p0.y + 2*s*t*pco.y + t*t*p1.y,
		)
	);
};

/*
| Scales the qcurve (pc0) to p0/p1.
*/
def.proto.scaleTo =
	function( p0, p1 )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( p0.ti2ctype !== Point ) throw new Error( );
/**/	if( p1.ti2ctype !== Point ) throw new Error( );
/**/}

	const tp0 = this.p0;
	const tp1 = this.p1;
	const tpco = this.pco;

	// FIXME use this.lazy values
	const phi0 = atan2( tpco.y - tp0.y, tpco.x - tp0.x );
	const phi1 = atan2( tpco.y - tp1.y, tpco.x - tp1.x );

	const sin0 = sin( phi0 );
	const cos0 = cos( phi0 );
	const sin1 = sin( phi1 );
	const cos1 = cos( phi1 );

	const l0cb =
		( ( p1.x - p0.x )*sin1 - ( p1.y - p0.y )*cos1 )
		/ ( sin1*cos0 - sin0*cos1 );
	const x = p0.x + cos0 * l0cb;
	const y = p0.y + sin0 * l0cb;

	const pco = Point.XY( x, y );
	return Self.P0PcoP1( p0, pco, p1 );
};

/*
| Splits the qcurve into two.
| Return a qcurve_list.
|
| ~t: split at this point
*/
def.proto.split =
	function( t )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( t ) !== 'number' ) throw new Error( );
/**/	if( !( t > 0 && t < 1 ) ) throw new Error( );
/**/}

	throw new Error( );

	/*
	const list = [ ];
	this._split( list, t, 0, 0 );
	return QCurveList.create( 'list:init', list );
	*/
};

/*
| Moves the qcurve by p or x/y.
*/
def.proto.sub =
	function( /* p or x,y */ )
{
	const p0 = this.p0;
	const pco = this.pco;
	const p1 = this.p1;

	return(
		this.create(
			'p0', p0.sub.apply( p0, arguments ),
			'pco', pco.sub.apply( pco, arguments ),
			'p1', p1.sub.apply( p1, arguments ),
		)
	);
};

/*
| Returns a transformed qcurve.
*/
def.proto.transform =
	function( transform )
{
	return(
		this.create(
			'p0', this.p0.transform( transform ),
			'pco', this.pco.transform( transform ),
			'p1', this.p1.transform( transform ),
		)
	);
};

/*
| The zone of the qcurve (aka bounding box)
*/
def.lazy.zone =
	function( )
{
	const p0 = this.p0;
	const pco = this.pco;
	const p1 = this.p1;

	const p0x = p0.x;
	const p0y = p0.y;
	const pcox = pco.x;
	const pcoy = pco.y;
	const p1x = p1.x;
	const p1y = p1.y;

	let pmix = min( p0x, p1x );
	let pmiy = min( p0y, p1y );
	let pmax = max( p0x, p1x );
	let pmay = max( p0y, p1y );

	// testing extrema:
	// derivative = 2((pco-p0)(1-t)+(p1-pco)(t) ) = 0
	// (pco-p0)(1-t)+(p1-pco)(t)=0
	// pco-p0-pco*t+p0*t+p1*t-pco*t=0
	// t*(-pco+p0+p1-pco)=-pco+p0
	// t = (p0-pco)/(p0+p1-2pco)

	const dx = p0x+p1x-2*pcox;
	const dy = p0y+p1y-2*pcoy;

	if( dx !== 0 )
	{
		let t = (p0x-pcox)/dx;
		if( t > 0 && t < 1 )
		{
			const s = 1 - t;
			const qx = s*s*p0x + 2*s*t*pcox + t*t*p1x;
			pmix = min( pmix, qx );
			pmax = max( pmax, qx );
		}
	}

	if( dy !== 0 )
	{
		let t = (p0y-pcoy)/dy;
		if( t > 0 && t < 1 )
		{
			const s = 1 - t;
			const qy = s*s*p0y + 2*s*t*pcoy + t*t*p1y;
			pmix = min( pmix, qy );
			pmax = max( pmax, qy );
		}
	}

	return Rect.PosPse( Point.XY( pmix, pmiy ), Point.XY( pmax, pmay ) );
};

/*
| Helper for split.
| This violates immutability for performance.
|
| ~array:   insert the parts in this array
| ~t:       split at this
| ~index:   operate at this offset.
| ~remove:  remove this number in splice
*/
def.proto._split =
	function( array, t, index, remove )
{
	const p0 = this.p0;
	const p1 = this.p1;
	const pco = this.pco;

	const p0x = p0.x;
	const p0y = p0.y;
	const pcox = pco.x;
	const pcoy = pco.y;
	const p1x = p1.x;
	const p1y = p1.y;

	const m = 1 - t;
	const mm = m * m;
	const tm2 = 2 * t * m;
	const tt = t * t;

	const pm =
		Point.XY(
			mm * p0x + tm2 * pcox + tt * p1x,
			mm * p0y + tm2 * pcoy + tt * p1y
		);

	array.splice(
		index, remove,
		Self.P0PcoP1( p0, Point.XY( m * p0x  + t * pcox, m * p0y  + t * pcoy ), pm ),
		Self.P0PcoP1( pm, Point.XY( m * pcox + t * p1x,  m * pcoy + t * p1y  ), p1 )
	);
};
