/*
| A blind section of a shape does nothing on a shape
| until it is enveloped.
|
| Used by shape.
*/
def.attributes =
{
	// the angle the line is going to have.
	angle: { type: [  'undefined', '< Figure/Angle/Types' ], json: true },

	// point the angle is at
	p: { type: 'Figure/Point', json: true },
};

def.json = true;

import { Self as Point } from '{Figure/Point}';
import { Self as Segment } from '{Figure/Path/Segment}';

const cos = Math.cos;
const sin = Math.sin;

/*
| Moves the shape section by p or x/y.
*/
def.proto.add =
	function( /* p or x,y */ )
{
	const p = this.p;
	return this.create( 'p', p.add.apply( p, arguments ) );
};

/*
| Returns a line with distance to this blind.
*/
def.lazyFunc.envelope =
	function( d )
{
	const an = this.angle;
	const p = this.p;
	const ra1 = an.ccw08.radians;
	const ra2 = an.ccw08.ccw08.ccw08.radians;
	const p0 = Point.XY( p.x + d * cos( ra1 ), p.y - d * sin( ra1 ) );
	const p1 = Point.XY( p.x + d * cos( ra2 ), p.y - d * sin( ra2 ) );
	return Segment.P0P1( p0, p1 );
};

/*
| Intersects this section with an figure
*/
def.proto.intersects = function( figure ) { return; };

/*
| Adapters.
*/
def.lazy.p0 = function( ) { return this.p; };
def.lazy.p1 = function( ) { return this.p; };

/*
| Returns a transformed shape section.
*/
def.proto.transform =
	function( transform )
{
	return this.create( 'p', this.p.transform( transform ) );
};
