/*
| A generic geometric shape built by a path.
*/
def.attributes =
{
	// true if the path is closed
	closed: { type: 'boolean', json: true },

	// radial gradient radius
	gradientR1: { type: [ 'undefined', 'number' ], json: true },

	// parts of the path
	parts: { type: 'Figure/Path/Parts', json: true },

	// center point
	pc: { type: [ 'undefined', 'Figure/Point' ], json: true },
};

def.json = true;

import { Self as Angle         } from '{Figure/Angle/Self}';
import { Self as Bezier        } from '{Figure/Path/Bezier}';
import { Self as Blind         } from '{Figure/Path/Blind}';
import { Self as Display       } from '{Display/Canvas/Self}';
import { Self as Parts         } from '{Figure/Path/Parts}';
import { Self as Point         } from '{Figure/Point}';
import { Self as QBend         } from '{Figure/Path/QBend}';
import { Self as QCurve        } from '{Figure/Path/QCurve}';
import { Self as Rect          } from '{Figure/Shape/Rect}';
import { Self as Segment       } from '{Figure/Path/Segment}';
import { Self as TransformBase } from '{Transform/Base}';

const trunc = Math.trunc;

// Default tolance for tolerant epsilon
const epsilonTolerant = 1e-3;

/*
| Angle at p0.
*/
def.lazy.angle0 =
	function( )
{
	return this.parts.first.angle0;
};

/*
| Angle at p1.
*/
def.lazy.angle1 =
	function( )
{
	return this.parts.last.angle1;
};

/*
| Moves the shape by p or x,y.
*/
def.proto.add =
	function( /* p or x,y */ )
{
	const list = [ ];

	for( let part of this.parts )
	{
		list.push( part.add.apply( part, arguments ) );
	}

	return this.create( 'parts', Parts.create( 'list:init', list ) );
};

/*
| Chips off a part of the bezier.
|
| ~t0: from this at (0..1)
| ~t1: to this at (0..1)
*/
def.proto.chip =
	function( t0, t1 )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( t0 > t1 ) throw new Error( );
/**/}

	if( t0 === t1 ) return undefined;
	if( t0 === 0 && t1 === 1 ) return this;

	const parts = this.parts;
	const length = parts.length;
	const pl = 1 / length;

	const o0 = t0 / pl;
	const ot0 = trunc( o0 );
	const st0 = o0 - ot0;

	const o1 = t1 / pl;
	const ot1 = trunc( o1 );
	const st1 = o1 - ot1;

	if( ot0 === ot1 )
	{
		return parts.get( ot0 ).chip( st0, st1 );
	}

	const list = [ ];
	if( st0 < 1 )
	{
		list.push( parts.get( ot0 ).chipUpper( st0 ) );
	}

	for( let a = ot0 + 1; a < ot1; a++ )
	{
		list.push( parts.get( a ) );
	}

	if( st1 > 0 && ot1 < length )
	{
		list.push( parts.get( ot1 ).chipLower( st1 ) );
	}

	return(
		Self.create(
			'parts', Parts.create( 'list:init', list ),
			'closed', false
		)
	);
};

/*
| Trims away the upper part from 0 to t.
| Return t to 1.
|
| ~t: split at this.
*/
def.proto.chipLower =
	function( t )
{
	return this.chip( 0, t );
};

/*
| Chips off the upper part from t to 1.
|
| ~t: split at this.
*/
def.proto.chipUpper =
	function( t )
{
	return this.chip( t, 1 );
};

/*
| Returns a shape enveloping this shape by +/- distance.
|
| ~d: distance
*/
def.lazyFunc.envelope =
	function( d )
{
/**/if( CHECK && typeof( d ) !== 'number' ) throw new Error( );

	if( d === 0 ) return this;

	const parts = this.parts;
	const list = [ ];
	// first envelopes each part
	for( let part of parts ) list.push( part.envelope( d ) );

	// then intersects the envelopes
	let c0 = list[ 0 ];
	c0 = list[ 0 ] =
		c0.create(
			'p0', c0.line.intersectsPoint( list[ list.length -1 ].line ),
			'p1', c0.line.intersectsPoint( list[ 1 ].line )
		);

	let p1 = c0.p1;
	for( let a = 1, alen = list.length; a < alen; a++ )
	{
		const p0 = p1;
		const ca = list[ a ];
		if( a + 1 < alen )
		{
			p1 = ca.line.intersectsPoint( list[ a + 1 ].line );
		}
		else
		{
			p1 = c0.p0;
		}

		if( p0 && p1 )
		{
			list[ a ] = ca.create( 'p0', p0, 'p1', p1 );
		}
		else
		{
			console.log( 'FIXME' );
		}
	}

	return this.create( 'parts', Parts.create( 'list:init', list ) );
};

/*
| Creates a path joining slightly misplaced segments.
|
| ~list:    the list (Array) of path segments (will be altered!)
| ~closed:  true if the path ought to be closed.
| ~epsilon: if defined use this as epsilon otherwise uses default
|           (only used when CHECKing)
*/
def.static.EpsilonTolerant =
	function( list, closed, epsilon )
{
/**/if( CHECK ) epsilon = epsilon || epsilonTolerant;

	let pre = closed && list[ list.length - 1 ];
	for( let a = 0, alen = list.length, ap = alen - 1; a < alen; a++ )
	{
		const cur = list[ a ];

		if( pre )
		{
			const p1 = pre.p1;
			const p0 = cur.p0;
			if( p0 !== p1 )
			{
				// fails building?
				if( p1.distanceOfPoint( p0 ) >= epsilon )
				{
					return undefined;
				}

				const p = Point.XY( ( p0.x + p1.x ) / 2, ( p0.y + p1.y ) / 2 );
				list[ ap ] = list[ ap ].create( 'p1', p );
				list[ a ] = list[ a ].create( 'p0', p );
			}
		}

		pre = cur;
		ap = a;
	}

	return Self.create( 'parts', Parts.create( 'list:init', list ), 'closed', closed );
};

/*
| Finds the intersections of the path with something else.
| Returns an array with doublet at values.
|
| ~figure:      the figure to find intersections with
| ~extended:    if defined figures are to be extended beyond their end.
| ~flip:        if defined flips results
| ~resultArray: if defined appends intersections to this array
|               OPTING OUT IMMUTABILITY!
*/
def.proto.intersectsAt =
	function( figure, extended, flip, resultArray )
{
	if( this === figure )
	{
		return this.selfIntersectsAt( resultArray );
	}

	if( !resultArray ) resultArray = [ ];

	const parts = this.parts;
	const pl = 1 / parts.length;

	let plo = 0;
	for( let part of parts )
	{
		const rao = resultArray.length + ( flip ? 1 : 0 );
		part.intersectsAt( figure, extended, flip, resultArray );

		// corrects the results
		for( let b = rao, blen = resultArray.length; b < blen; b+=2 )
		{
			resultArray[ b ] = resultArray[ b ] * pl + plo;
		}
		plo += pl;
	}

	return resultArray;
};

/*
| Intersects this line segment with another figure.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	const parts = this.parts;
	for( let part of parts )
	{
		const pi = part.intersectsPoint( figure );
		if( pi )
		{
			return pi;
		}
	}

	// no part intersected
	return undefined;
};

/*
| Adapters.
*/
def.lazy.p0 =
	function( )
{
	return this.parts.first.p0;
};

def.lazy.p1 =
	function( )
{
	return this.parts.last.p1;
};

/*
| Creates a path by defining the points as plan
| as free strings.
|
| ~args: free string list:
|
| Free string options:
|
| 'line', ['fly'], point or x,y or 'close'
|		makes a line segment
|		'fly' draws no line (but filling edge)
|
| 'pc', point or x,y
|		center point
|
| 'qbend', ['ccw'], point or x,y or 'close'
|		makes a quater bend
|       'ccw' goes counter clockwise from here on
|		FUTURE allow flying clock switching
|
| 'start' point or x,y or 'close'
|		starts the path here
|    	point or x and y
*/
def.static.Plan =
	function( ...args )
{
	const list = [ ];

	// center point
	let pc;

	// loop variables
	let a = 0, alen = args.length;

	// last point in plan
	let p0;

	// starting point
	let ps;

	// true if a second iteration is needed
	let again = false;

	// true if the path is closed
	let closed = false;

	while( a < alen )
	{
		switch( args[ a ] )
		{
			case 'bezier':
			{
				const pco0 = args[ a + 1 ];
				const pco1 = args[ a + 2 ];
				let p1     = args[ a + 3 ];
				a += 4;

				if( p1 === 'close' )
				{
					p1 = ps;
					closed = true;
				}

				list.push( Bezier.create( 'p0', p0, 'p1', p1, 'pco0', pco0, 'pco1', pco1 ) );
				p0 = p1;
				continue;
			}

			case 'blind':
			{
				again = true;
				list.push( Blind.create( 'p', p0 ) );
				a++;
				continue;
			}

			case 'line':
			{
				let a1 = args[ a + 1 ];
				let fly, p1;
				if( a1 === 'fly' )
				{
					fly = true;
					a++;
					a1 = args[ a + 1 ];
				}

				if( a1 === 'close' )
				{
					p1 = ps;
					closed = true;
					a += 2;
				}
				else if( a1.ti2ctype === Point )
				{
					p1 = a1;
					a += 2;
				}
				else
				{
					p1 = Point.XY( a1, args[ a + 2 ] );
					a += 3;
				}

				list.push( Segment.create( 'p0', p0, 'p1', p1, 'fly', fly ) );
				p0 = p1;
				continue;
			}

			case 'pc':
			{
/**/			if( CHECK && pc ) throw new Error( );

				pc = args[ a + 1 ];
				if( pc.ti2ctype === Point ) { a += 2; continue; }
				pc = Point.XY( pc, args[ a + 2 ] );
				a += 3;
				continue;
			}

			case 'qbend':
			{
				let a1 = args[ a + 1 ];
				let ccw = false;
				let p1;
				if( a1 === 'ccw' )
				{
					ccw = true;
					a++;
					a1 = args[ a + 1 ];
				}

				if( a1 === 'close' )
				{
					p1 = ps;
					closed = true;
					a += 2;
				}
				else if( a1.ti2ctype === Point )
				{
					p1 = a1;
					a += 2;
				}
				else
				{
					p1 = Point.XY( a1, args[ a + 2 ] );
					a += 3;
				}

				list.push( QBend.create( 'p0', p0, 'p1', p1, 'ccw', ccw ) );
				p0 = p1;
				continue;
			}

			case 'qcurve':
			{
				const pco = args[ a + 1 ];
				let p1 = args[ a + 2 ];
				if( p1 === 'close' )
				{
					p1 = ps;
					closed = true;
				}
				a += 3;
				list.push( QCurve.create( 'p0', p0, 'p1', p1, 'pco', pco ) );
				p0 = p1;
				continue;
			}

			case 'start':
			{
/**/			if( CHECK && list.length !== 0 ) throw new Error( );

				let a1 = args[ a + 1 ];
				if( a1.ti2ctype === Point )
				{
					p0 = a1;
					a += 2;
				}
				else
				{
					p0 = Point.XY( a1, args[ a + 2 ] );
					a += 3;
				}
				ps = p0;
				continue;
			}

			default: throw new Error( );
		}
	}

	//a + ( (b+pi)/2 - a/2
	//a/2 + ( (b+pi)/2

	if( again )
	{
		for( let a = 0, alen = list.length; a < alen; a++ )
		{
			const cs = list[ a ];
			if( cs.ti2ctype !== Blind )
			{
				continue;
			}
			const ps = list[ a - 1 >= 0 ? a - 1 : alen - 1 ];
			const ns = list[ a + 1 < alen ? a + 1 : 0 ];
			const an = Angle.Radians( ( ps.angle.radians + ns.angle.radians ) / 2) ;
			list[ a ] = cs.create( 'angle', an );
		}
	}

	return(
		Self.create(
			'closed', closed,
			'parts', Parts.create( 'list:init', list ),
			'pc', pc,
		)
	);
};

/*
| Returns point at t (from 0 to 1 ).
*/
def.lazyFunc.pointAt =
	function( t )
{
/**/if( CHECK )
/**/{
/**/	if( !( t >= 0 && t <= 1 ) ) throw new Error( );
/**/}

	const parts = this.parts;
	const pl = 1 / parts.length;
	const o = t / pl;
	const ot = trunc( o );
	const pt = o - ot;

	return parts.get( ot ).pointAt( pt );
};

/*
| Returns self intersections.
*/
def.proto.selfIntersectsAt =
	function( resultArray )
{
	if( !resultArray ) resultArray = [ ];
	const parts = this.parts;
	const length = parts.length;
	const pl = 1 / length;

	let poa = 0;
	for( let a = 0; a < length; a++ )
	{
		const apart = parts.get( a );
		let pob = poa + pl;
		for( let b = a + 1; b < length; b++ )
		{
			const bsec = parts.get( b );
			const isRight = b === ( a + 1 ) % length;
			const isLeft = a === 0 && b === length - 1 && this.closed;

			const rao = resultArray.length;
			apart.intersectsAt( bsec, false, false, resultArray, isLeft, isRight );

			// corrects the results
			for( let r = rao, rlen = resultArray.length; r < rlen; r+=2 )
			{
				resultArray[ r ] = resultArray[ r ] * pl + poa;
				resultArray[ r + 1 ] = resultArray[ r + 1 ] * pl + pob;
			}
			pob += pl;
		}
		poa += pl;
	}
	return resultArray;
};

/*
| Moves the shape by p or x,y.
*/
def.proto.sub =
	function( /* p or x,y */ )
{
	const list = [ ];

	for( let part of this.parts )
	{
		list.push( part.sub.apply( part, arguments ) );
	}

	return this.create( 'parts', Parts.create( 'list:init', list ) );
};

/*
| Splits the path at 't'.
|
| ~t:      split at this
| ~array:  insert the parts in this array
| ~index:  operate at this offset.
| ~remove: remove this number in splice
*/
def.proto.split =
	function( t, array, index, remove )
{
/**/if( CHECK )
/**/{
/**/	if( !( t >= 0 && t <= 1 ) ) throw new Error( );
/**/}

	const parts = this.parts;
	const length = parts.length;
	const pl = 1 / length;
	const o = t / pl;
	const ot = trunc( o );
	const st = o - ot;

	const list1 = this._list.slice( 0, ot ); // FIXME
	const os = this.get( ot );
	os.split( st, list1 );

	const list2 = [ list1.pop( ) ];
	for( let a = ot + 1; a < length; a++ ) list2.push( this.get( a ) );

	const s0 = Self.create( 'list:init', list1, 'closed', false );
	const s1 = Self.create( 'list:init', list2, 'closed', false );

	if( array )
	{
		if( index === undefined ) array.push( s0, s1 );
		else array.splice( index || 0, remove || 0, s0, s1 );
	}
	else array = [ s0, s1 ];
	return array;
};

/*
| Returns a transformed shape.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	const list = [ ];
	for( let part of this.parts )
	{
		list.push( part.transform( transform ) );
	}
	return this.create( 'parts', Parts.create( 'list:init', list ) );
};

/*
| Returns true if p is within the shape.
*/
def.proto.within =
	function( p )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return Display.helper.within( p, this );
};

/*
| The zone of the path.
*/
def.lazy.zone =
	function( )
{
	const parts = this.parts;
	let x0, y0, x1, y1;

	{
		const part0 = parts.get( 0 );
		const sz = part0.zone;
		const p0 = sz.pos;
		const p1 = sz.pse;
		x0 = p0.x;
		y0 = p0.y;
		x1 = p1.x;
		y1 = p1.y;
	}

	for( let a = 1, alen = parts.length; a < alen; a++ )
	{
		const part = parts.get( a );
		const sz = part.zone;
		const p0 = sz.pos;
		const p1 = sz.pse;
		const sx0 = p0.x;
		const sy0 = p0.y;
		const sx1 = p1.x;
		const sy1 = p1.y;

		if( sx0 < x0 ) x0 = sx0;
		if( sy0 < y0 ) y0 = sy0;
		if( sx1 > x1 ) x1 = sx1;
		if( sy1 > y1 ) y1 = sy1;
	}

	return(
		Rect.PosPse(
			Point.XY( x0, y0 ),
			Point.XY( x1, y1 ),
		)
	);
};

/*
| Extra checking.
*/
def.proto._check =
	function( )
{
/**/if( CHECK )
/**/{
/**/	const parts = this.parts;
/**/	let pre = this.closed && parts.last;
/**/	for( let part of parts )
/**/	{
/**/		if( pre && part.p0 !== pre.p1 ) throw new Error( );
/**/		pre = part;
/**/	}
/**/}
};
