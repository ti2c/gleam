/*
| A line segment.
*/
def.attributes =
{
	// from point
	p0: { type: 'Figure/Point', json: true },

	// to point
	p1: { type: 'Figure/Point', json: true },

	// doesn't draw a line ("border") as glint
	fly: { type: [ 'undefined', 'boolean' ], json: true },
};

def.json = true;

import { Self as Angle     } from '{Figure/Angle/Self}';
import { Self as Bezier    } from '{Figure/Path/Bezier}';
import { Self as Ellipse   } from '{Figure/Shape/Ellipse}';
import { Self as Line      } from '{Figure/Path/Line}';
import { Self as Path      } from '{Figure/Path/Self}';
import { Self as PathParts } from '{Figure/Path/Parts}';
import { Self as Point     } from '{Figure/Point}';
import { Self as QBend     } from '{Figure/Path/QBend}';
import { Self as QCurve    } from '{Figure/Path/QCurve}';
import { Self as Ray       } from '{Figure/Path/Ray}';
import { Self as Rect      } from '{Figure/Shape/Rect}';
import { Self as RectRound } from '{Figure/Shape/RectRound}';
import { Self as Segment   } from '{Figure/Path/Segment}';

const abs = Math.abs;
const max = Math.max;
const min = Math.min;

/*
| Moves the line segment by p or x/y.
|
| ~arguments: p or x,y
*/
def.proto.add =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return(
		this.create(
			'p0', p0.add.apply( p0, arguments ),
			'p1', p1.add.apply( p1, arguments )
		)
	);
};

/*
| Returns the angle from p0 to p1.
*/
def.lazy.angle =
	function( )
{
	return Angle.P0P1( this.p0, this.p1 );
};

/*
| Angle at p0.
*/
def.lazy.angle0 =
	function( )
{
	return this.angle;
};

/*
| Angle at p1.
*/
def.lazy.angle1 =
	function( )
{
	return this.angle;
};

/*
| Chips of a part of the segment.
|
| ~t0: from this at (0..1)
| ~t1: to this at (0..1)
*/
def.proto.chip =
	function( t0, t1 )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( t0 > t1 ) throw new Error( );
/**/}

	if( t0 === 0 && t1 === 1 )
	{
		return this;
	}

	if( t0 === t1 )
	{
		return undefined;
	}

	const p0 = this.pointAt( t0 );
	const p1 = this.pointAt( t1 );

	return Self.P0P1( p0, p1 );
};

/*
| Trims away the upper part from 0 to t.
|
| ~t: split at this
*/
def.proto.chipLower =
	function( t )
{
	if( t === 1 ) return this;
	return Self.P0P1( this.p0, this.pointAt( t ) );
};

/*
| Chips off the upper part from t to 1.
|
| ~t: split at this
*/
def.proto.chipUpper =
	function( t )
{
	if( t === 0 ) return this;
	return Self.P0P1( this.pointAt( t ), this.p1 );
};

/*
| Finds the shortest connection between two shapes/points.
|
| ~sp0: shape0 or point0
| ~sp1: shape1 or point1
*/
def.static.Connection =
	function( sp0, sp1 )
{
/**/if( CHECK && !sp0 || !sp1 ) throw new Error( );

	const pc0 = sp0.ti2ctype === Point ? sp0 : sp0.pc;
	const pc1 = sp1.ti2ctype === Point ? sp1 : sp1.pc;

	// the projection points
	let p0, p1;
	let segmentC;

	if( sp0.ti2ctype === Point ) p0 = sp0;
	else if( sp0.within( pc1 ) ) p0 = pc0;
	else
	{
		segmentC = Segment.P0P1( pc0, pc1 );
		p0 = sp0.intersectsPoint( segmentC );
		if( !p0 ) p0 = pc0;
	}

	if( sp1.ti2ctype === Point ) p1 = sp1;
	else if( sp1.within( pc0 ) ) p1 = pc1;
	else
	{
		if( !segmentC )
		{
			segmentC = Segment.P0P1( pc0, pc1 );
		}

		p1 = sp1.intersectsPoint( segmentC );

		if( !p1 )
		{
			p1 = pc0;
		}
	}

	return Self.P0P1( p0, p1 );
};

/*
| Returns true if p0p1 intersects with q0q1.
*/
def.static.doesIntersect4Points =
	function( p0, p1, q0, q1 )
{
	const o0 = Self.orientation3Points( p0, p1, q0 );
	const o1 = Self.orientation3Points( p0, p1, q1 );
	const o2 = Self.orientation3Points( q0, q1, p0 );
	const o3 = Self.orientation3Points( q0, q1, p1 );

	if( o0 !== o1 && o2 !== o3 ) return true;
	if( o0 === 0 && Self.onColinearSegment( p0, q0, p1 ) ) return true;
	if( o1 === 0 && Self.onColinearSegment( p0, q1, p1 ) ) return true;
	if( o2 === 0 && Self.onColinearSegment( q0, p0, q1 ) ) return true;
	if( o3 === 0 && Self.onColinearSegment( q0, p1, q1 ) ) return true;

	return false;
};

/*
| Returns the distance of a point to the line segment.
|
| ~a1, ~a2: p or x/y
*/
def.proto.distanceOfPoint =
	function( a1, a2 )
{
	let x, y;

	if( arguments.length === 1 )
	{
		x = a1.x;
		y = a1.y;
	}
	else
	{
		x = a1;
		y = a2;
	}

	const x0 = this.p0.x;
	const y0 = this.p0.y;
	const x1 = this.p1.x;
	const y1 = this.p1.y;

	const a = x - x0;
	const b = y - y0;
	const c = x1 - x0;
	const d = y1 - y0;

	const dot = a * c + b * d;
	const len2 = c * c + d * d;
	const param = len2 !== 0 ? dot / len2 : -1;
	let xx, yy;

	if( param < 0 )
	{
		xx = x0;
		yy = y0;
	}
	else if(param > 1 )
	{
		xx = x1;
		yy = y1;
	}
	else
	{
		xx = x0 + param * c;
		yy = y0 + param * d;
	}

	const dx = x - xx;
	const dy = y - yy;

	return Math.sqrt( dx * dx + dy * dy );
};

/*
| Returns a line segment with distance d next to this line segment.
*/
def.lazyFunc.envelope =
	function( d )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 1 ) throw new Error( );
/**/	if( typeof( d ) !== 'number' ) throw new Error( );
/**/	if( Number.isNaN( d ) ) throw new Error( );
/**/}

	// xx + yy = dd
	// k = y/x
	// y = kx
	// xx + kkxx = dd
	// xx( 1 + kk ) = dd
	// x=sqrt( dd / ( 1 + kk ) )
	//
	// -or-
	//
	// yy = dd - xx
	// yy = dd - yy/kk
	//  1 = dd/yy - 1/kk
	//  1 + 1/kk = dd/yy
	//  1 / ( 1 + 1/kk ) == yy / dd
	//  yy = dd / ( 1 + 1/kk )

	const tilt = this.tilt;
	const k = 1 / this.k;
	let dx, dy;

	if( k > -100000 && k < 100000 )
	{
		dx = tilt * Math.sqrt( d*d / ( 1 + k * k ) );
		dy = -k * dx;
	}
	else
	{
		dy = -tilt * Math.sqrt( d*d / ( 1 + 1 / ( k * k ) ) );
		dx = dy / k;
	}

	return(
		d > 0
		? this.add( dx, dy )
		: this.add( -dx, -dy )
	);
};

/*
| Intersects this line segment with another figure.
|
| ~figure:      the figure to find intersections with
| ~extended:    if defined figures are to be extended beyond their end.
| ~flip:        if defined flips results
| ~resultArray: if defined appends intersections to this array
| ~isLeft:      true if figure is left of this
| ~isRight:     true if figure is right of this
*/
def.proto.intersectsAt =
	function( figure, extended, flip, resultArray, isLeft, isRight )
{
	if( figure.ti2ctype === Self )
	{
		// neighbouring segments dont intersect and segments don't selfintersect
		if( isLeft || isRight || figure === this ) return resultArray;

		return this._intersectsSegmentAt( figure, extended, flip, resultArray );
	}

	if(
		figure.ti2ctype === Bezier
		|| figure.ti2ctype === Path
		|| figure.ti2ctype === QBend
		|| figure.ti2ctype === QCurve
	)
	{
		return figure.intersectsAt( this, extended, !flip, resultArray, isRight, isLeft );
	}

	throw new Error( );
};

/*
| Intersects this line segment with another figure.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	if( figure.ti2ctype === Line )
	{
		return this._intersectsLinePoint( figure );
	}

	if( figure.ti2ctype === Self )
	{
		return this._intersectsSegmentPoint( figure );
	}

	if( figure.ti2ctype === Ray )
	{
		return this._intersectsRayPoint( figure );
	}

	if(
		figure.ti2ctype === Ellipse
		|| figure.ti2ctype === QBend
		|| figure.ti2ctype === Rect
		|| figure.ti2ctype === RectRound
	)
	{
		return figure.intersectsPoint( this );
	}

	throw new Error( );
};

/*
| Slope of line segment.
*/
def.lazy.k =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	const dx = p1.x - p0.x;
	const dy = p1.y - p0.y;
	return dy / dx;
};

/*
| Length of line segment.
*/
def.lazy.length =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return Math.abs( p0.distanceOfPoint( p1 ) );
};

/*
| Unlimited line of segment.
*/
def.lazy.line =
	function( )
{
	return Line.P0P1( this.p0, this.p1 );
};

/*
| True if p1 is between p0 and p2.
*/
def.static.onColinearSegment =
	function( p0, p1, p2 )
{
	return(
		p1.x <= max( p0.x, p2.x )
		&& p1.x >= min( p0.x, p2.x )
		&& p1.y <= max( p0.y, p2.y )
		&& p1.y >= min( p0.y, p2.y )
	);
};

/*
|  0: p0, p1 and p2 are colinear
|  1: clockwise
| -1: counterclockwise
*/
def.static.orientation3Points =
	function( p0, p1, p2 )
{
	const v = (p1.y - p0.y) * (p2.x - p1.x) - (p1.x - p0.x) * (p2.y - p1.y);
	if( v === 0 ) return 0;
	return( v > 0 ) ? 1: -1;
};

/*
| Creates by p0, angle, and length
*/
def.static.P0AngleLen =
	function( p0, angle, len )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 3 ) throw new Error( );
/**/	if( p0.ti2ctype !== Point ) throw new Error( );
/**/	if( typeof( len ) !== 'number' ) throw new Error( );
/**/}

	const p1 = Point.XY( p0.x + angle.cos * len, p0.y - angle.sin * len );
	return Self.P0P1( p0, p1 );
};

/*
| Shortcut.
*/
def.static.P0P1 =
	( p0, p1 ) =>
	Self.create( 'p0', p0, 'p1', p1 );

/*
| Shortcut.
*/
def.static.P0P1Fly =
	( p0, p1 ) =>
	Self.create( 'p0', p0, 'p1', p1, 'fly', true );

/*
| The point at center.
*/
def.lazy.pc =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return Point.XY( ( p0.x + p1.x ) / 2, ( p0.y + p1.y ) / 2 );
};

/*
| Returns point at t ( from 0 to 1 ).
*/
def.proto.pointAt =
	function( t )
{
	const angle = this.angle;
	const tlen = t * this.length;
	const p0 = this.p0;
	return Point.XY( p0.x + tlen * angle.cos, p0.y - tlen * angle.sin );
};

/*
| Returns the ray starting from p1 going into the opposite direction.
*/
def.lazy.rayP1Mirror =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;

	return(
		Ray.P0P1(
			p1,
			Point.XY( 2 * p1.x - p0.x, 2 * p1.y - p0.y )
		)
	);
};

/*
| Reverses the line segment.
| (it will envelope in the other direction).
*/
def.lazy.reverse =
	function( )
{
	const r = this.create( 'p0', this.p1, 'p1', this.p0 );
	ti2c.aheadValue( r, 'reverse', this );
	return r;
};

/*
| Splits the segment at 't'.
|
| ~t:      split at this
| ~array:  insert the parts in this array
| ~index:  operate at this offset.
| ~remove: remove this number in splice
*/
def.proto.split =
	function( t,  array, index, remove )
{
/**/if( CHECK )
/**/{
/**/	if( typeof( t ) !== 'number' ) throw new Error( );
/**/	if( !( t > 0 && t < 1 ) ) throw new Error( );
/**/}

	const p = this.pointAt( t );
	const s0 = Self.P0P1( this.p0, p );
	const s1 = Self.P0P1( p, this.p1 );

	if( array )
	{
		if( index === undefined ) array.push( s0, s1 );
		else array.splice( index || 0, remove || 0, s0, s1 );
	}
	else array = [ s0, s1 ];

	return array;
};


/*
| Moves the line segment by p or x/y.
|
| ~arguments: p or x,y
*/
def.proto.sub =
	function( )
{
	const p0 = this.p0;
	const p1 = this.p1;
	return(
		this.create(
			'p0', p0.sub.apply( p0, arguments ),
			'p1', p1.sub.apply( p1, arguments )
		)
	);
};

/*
| Returns a path making this segment thick.
*/
def.lazyFunc.thickPath =
	function( width )
{
/**/if( CHECK && typeof( width ) !== 'number' ) throw new Error( );

	const d = width / 2;
	const e0 = this.envelope( d );
	const e1 = this.reverse.envelope( d );
	return(
		Path.create(
			'closed', true,
			'pc', this.pc,
			'parts',
				PathParts.Elements(
					e0,
					Self.P0P1Fly( e0.p1, e1.p0 ),
					e1,
					Self.P0P1Fly( e1.p1, e0.p0 ),
				)
		)
	);
};

/*
| Returns the "direction" of the slope.
| Used by enveloping.
*/
def.lazy.tilt =
	function( )
{
	const dy = this.p1.y - this.p0.y;
	if( dy < 0 ) return -1;
	if( dy > 0 ) return  1;
	return this.p1.x - this.p0.x > 0 ? 1 : -1;
};

/*
| Returns a transformed line segment.
*/
def.proto.transform =
	function( transform )
{
	return(
		this.create(
			'p0', this.p0.transform( transform ),
			'p1', this.p1.transform( transform )
		)
	);
};

/*
| Returns true if point is at most 'distance' far away.
| This may be faster than directly 'distanceOfPoint'.
*/
def.proto.withinDistance =
	function( p, distance )
{
	const zd = this.zone.envelope( distance );
	if( !zd.within( p ) ) return false;
	const d = this.distanceOfPoint( p );
	return d <= distance;
};

/*
| Returns true if a point (x/y) is within the limits of the path,
| assuming it is on the path.
|
| Always true for lines.
| True for segments if within segment.
| True for rays if on the ray side.
|
| ~x/y, or p: point to test
*/
def.proto.withinLimits =
	function( a1, a2 )
{
	let x, y;
	if( arguments.length === 1 ) { x = a1.x; y = a1.y; }
	else{ x = a1; y = a2; }

	const p0 = this.p0;
	const p1 = this.p1;
	const x0 = p0.x;
	const x1 = p1.x;

	if( x0 !== x1 )
	{
		return ( x >= x0 && x <= x1 ) || ( x >= x0 && x <= x1 );
	}
	else
	{
		const y0 = p0.y;
		const y1 = p1.y;
		return ( y >= y0 && y <= y1 ) || ( y >= y0 && y <= y1 );
	}
};

/*
| The zone of the line segment.
*/
def.lazy.zone =
	function( )
{
	return Rect.Arbitrary( this.p0, this.p1 );
};

/*
| Intersects this line segment with a line.
|
| ~line:     the line to intersect with
*/
def.proto._intersectsLinePoint =
	function( line )
{
/**/if( CHECK && line.ti2ctype !== Line ) throw new Error( );

	const p0 = this.p0;
	const p1 = this.p1;
	const p2 = line.p0;
	const p3 = line.p1;

	const x0 = p0.x, y0 = p0.y;
	const x1 = p1.x, y1 = p1.y;
	const x2 = p2.x, y2 = p2.y;
	const x3 = p3.x, y3 = p3.y;

	if( x0 === x2 && y0 === y2 ) return p0;
	if( x0 === x3 && y0 === y3 ) return p0;
	if( x1 === x2 && y1 === y2 ) return p1;
	if( x1 === x3 && y1 === y3 ) return p1;

	const den = ( x0 - x1 )*( y2 - y3 ) - ( y0 - y1 )*( x2 - x3 );
	if( den === 0 )
	{
		// doesn't intersect
		return undefined;
	}

	const a = x0*y1 - y0*x1;
	const b = x2*y3 - y2*x3;

	const x = ( a*( x2 - x3 ) - ( x0 - x1 )*b ) / den;
	const y = ( a*( y2 - y3 ) - ( y0 - y1 )*b ) / den;

	// checks if the intersection is on the segment
	return(
		this.withinLimits( x, y )
		? Point.XY( x, y )
		: undefined
	);
};

/*
| Intersects this line segment with a ray.
*/
def.proto._intersectsRayPoint =
	function( ray )
{
/**/if( CHECK && ray.ti2ctype !== Ray ) throw new Error( );

	const tp0 = this.p0;
	const tp1 = this.p1;
	const rp0 = ray.p0;
	const rp1 = ray.p1;

	const t0x = tp0.x, t0y = tp0.y;
	const t1x = tp1.x, t1y = tp1.y;
	const r0x = rp0.x, r0y = rp0.y;
	const r1x = rp1.x, r1y = rp1.y;

	if( t0x === r0x && t0y === r0y ) return tp0;
	if( t0x === r1x && t0y === r1y ) return tp0;
	if( t1x === r0x && t1y === r0y ) return tp1;
	if( t1x === r1x && t1y === r1y ) return tp1;

	const den = ( t0x - t1x )*( r0y - r1y ) - ( t0y - t1y )*( r0x - r1x );
	if( den === 0 )
	{
		// doesn't intersect
		return undefined;
	}
	const a = t0x * t1y - t0y * t1x;
	const b = r0x * r1y - r0y * r1x;

	let x, y;

	// in case one line is vertical the intersection must be on that x value
	if( t0x === t1x ) x = t0x;
	else if( r0x === r1x ) x = r0x;
	else x = ( a*( r0x - r1x ) - ( t0x - t1x )*b ) / den;

	// in case one line is horizontal the intersection must be on that y value
	if( t0y === t1y ) y = t0y;
	else if( r0y === r1y ) y = r0y;
	else y = ( a*( r0y - r1y ) - ( t0y - t1y )*b ) / den;

	// checks if the intersection is on this segments and on the ray's correct side
	if( t0x === t1x || r0x === r1x )
	{
		if(
			( x >= t0x && x <= t1x || x <= t0x && x >= t1x )
			&& ( x >= r0x && r1x >= r0x || x <= r0x && r1x <= r0x )
			&& ( y >= t0y && y <= t1y || y <= t0y && y >= t1y )
			&& ( y >= r0y && r1y >= r0y || y <= r0y && r1y <= r0y )
		) return Point.XY( x, y );
	}
	else
	{
		if(
			( x >= t0x && x <= t1x || x <= t0x && x >= t1x )
			&& (
				( x >= r0x && r1x >= r0x || x <= r0x && r1x <= r0x )
				// in vertical rays above line can be inaccurate
				|| ( y >= r0y && r1y >= r0y || y <= r0y && r1y <= r0y )
			)
		) return Point.XY( x, y );
	}

	// no valid intersection
	return undefined;
};

/*
| Returns the intersections of the segment with anoter.
|
| ~segment:     the figure to find intersections with
| ~extended:    if defined figures are to be extended beyond their end.
| ~flip:        if defined flips results
| ~resultArray: if defined push to this
*/
def.proto._intersectsSegmentAt =
	function( segment, extended, flip, resultArray )
{
/**/if( CHECK && segment.ti2ctype !== Self ) throw new Error( );

	const p0 = this.p0;
	const p1 = this.p1;
	const p2 = segment.p0;
	const p3 = segment.p1;

	const x0 = p0.x, y0 = p0.y;
	const x1 = p1.x, y1 = p1.y;
	const x2 = p2.x, y2 = p2.y;
	const x3 = p3.x, y3 = p3.y;

	const den = ( x0 - x1 )*( y2 - y3 ) - ( y0 - y1 )*( x2 - x3 );
	if( den === 0 )
	{
		return resultArray; // doesn't intersect
	}

	const a = x0*y1 - y0*x1;
	const b = x2*y3 - y2*x3;

	const x = ( a*( x2 - x3 ) - ( x0 - x1 )*b ) / den;
	const y = ( a*( y2 - y3 ) - ( y0 - y1 )*b ) / den;

	let at0, at1;
	let dx = x0 - x1;
	let dy = y0 - y1;

	if( abs( dx ) > abs( dy ) )
	{
		at0 = ( x0 - x ) / ( x0 - x1 );
	}
	else
	{
		at0 = ( y0 - y ) / ( y0 - y1 );
	}

	dx = x2 - x3;
	dy = y2 - y3;
	if( abs( dx ) > abs( dy ) )
	{
		at1 = ( x2 - x ) / ( x2 - x3 );
	}
	else
	{
		at1 = ( y2 - y ) / ( y2 - y3 );
	}

	if( !extended && ( at0 < 0 || at0 > 1 || at1 < 0 || at1 > 1 ) )
	{
		return resultArray;
	}

	if( !resultArray ) resultArray = [ ];

	if( !flip )
	{
		resultArray.push( at0, at1 );
	}
	else
	{
		resultArray.push( at1, at0 );
	}

	return resultArray;
};

/*
| Intersects this line segment with another line Segment.
*/
def.proto._intersectsSegmentPoint =
	function( segment )
{
/**/if( CHECK && segment.ti2ctype !== Self ) throw new Error( );

	const tp0 = this.p0;
	const tp1 = this.p1;
	const sp0 = segment.p0;
	const sp1 = segment.p1;

	const t0x = tp0.x, t0y = tp0.y;
	const t1x = tp1.x, t1y = tp1.y;
	const s0x = sp0.x, s0y = sp0.y;
	const s1x = sp1.x, s1y = sp1.y;

	if( t0x === s0x && t0y === s0y ) return tp0;
	if( t0x === s1x && t0y === s1y ) return tp0;
	if( t1x === s0x && t1y === s0y ) return tp1;
	if( t1x === s1x && t1y === s1y ) return tp1;

	const den = ( t0x - t1x )*( s0y - s1y ) - ( t0y - t1y )*( s0x - s1x );
	if( den === 0 )
	{
		// doesn't intersect
		return undefined;
	}

	const a = t0x * t1y - t0y * t1x;
	const b = s0x * s1y - s0y * s1x;

	let x, y;

	// in case one line is vertical the intersection must be on that x value
	if( t0x === t1x )
	{
		x = t0x;
	}
	else if( s0x === s1x )
	{
		x = s0x;
	}
	else
	{
		x = ( a*( s0x - s1x ) - ( t0x - t1x )*b ) / den;
	}

	// in case one line is horizontal the intersection must be on that y value
	if( t0y === t1y )
	{
		y = t0y;
	}
	else if( s0y === s1y )
	{
		y = s0y;
	}
	else
	{
		y = ( a*( s0y - s1y ) - ( t0y - t1y )*b ) / den;
	}

	// checks if the intersection is on the segments.
	if( t0x === t1x || s0x === s1x )
	{
		if(
			( x >= t0x && x <= t1x || x <= t0x && x >= t1x )
			&& ( x >= s0x && x <= s1x || x <= s0x && x >= s1x )
			&& ( y >= t0y && y <= t1y || y <= t0y && y >= t1y )
			&& ( y >= s0y && y <= s1y || y <= s0y && y >= s1y )
		) return Point.XY( x, y );
	}
	else
	{
		if(
			( x >= t0x && x <= t1x || x <= t0x && x >= t1x )
			&& ( x >= s0x && x <= s1x || x <= s0x && x >= s1x )
		) return Point.XY( x, y );
	}

	// no valid intersection
	return undefined;
};
