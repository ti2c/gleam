/*
| An ellipse.
*/
def.attributes =
{
	// center for gradient
	gradientPC: { type: [ 'undefined', 'Figure/Point' ] },

	// inner radius for circle gradients
	gradientR0: { type: [ 'undefined', 'number' ] },

	// outer radius for circle gradients
	gradientR1: { type: [ 'undefined', 'number' ] },

	// height of the ellipse
	height: { type: 'number', json: true },

	// width of the ellipse
	width: { type: 'number', json: true },

	// position
	pos: { type: 'Figure/Point', json: true },
};

def.json = true;

import { Self as Display       } from '{Display/Canvas/Self}';
import { Self as Path          } from '{Figure/Path/Self}';
import { Self as TransformBase } from '{Transform/Base}';

/*
| Returns a moved ellipse.
*/
def.proto.add =
	function( a0, a1 )
{
	return this.create( 'pos', this.pos.add( a0, a1 ) );
};

/*
| Shortcut to create an ellipse by specifying it's center and size.
*/
def.static.CenterSize =
	function( center, size )
{
	return(
		Self.create(
			'pos', center.add( -size.width / 2, -size.height / 2 ),
			'width', size.width,
			'height', size.height
		)
	);
};

/*
| Shortcut to create an ellipse by specifying it's center, width and height.
*/
def.static.CenterWidthHeight =
	function( center, width, height )
{
	return(
		Self.create(
			'pos',    center.add( -width / 2, -height / 2 ),
			'width',  width,
			'height', height
		)
	);
};

/*
| Returns the ellipse enveloped by d.
|
| ~d: distance
*/
def.proto.envelope =
	function( d )
{
	return(
		this.create(
			'pos', this.pos.add( -d, -d ),
			'width', this.width + 2 * d,
			'height', this.height + 2 * d
		)
	);
};

/*
| Gets the intersections with something else.
|
| FUTURE it might actually be faster to calculate it for the ellipse.
|
| ~figure: figure to intersect with
*/
def.proto.intersectsPoint =
	function( figure )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return this.path.intersectsPoint( figure );
};

/*
|'magic' number to approximate ellipses with beziers.
*/
def.static.magic = 0.551784;

/*
| Center point of an ellipse.
*/
def.lazy.pc =
	function( )
{
	return this.pos.add( this.width / 2, this.height / 2 );
};

/*
| East point.
*/
def.lazy.pe =
	function( )
{
	return this.pos.add( this.width, this.height / 2 );
};

/*
| North point.
*/
def.lazy.pn =
	function( )
{
	return this.pos.add( this.width / 2, 0 );
};

/*
| Shortcut to create an ellipse by specifying p and size.
*/
def.static.PosSize =
	function( pos, size )
{
	return Self.create( 'pos', pos, 'width', size.width, 'height', size.height );
};

/*
| Shortcut to create an ellipse by specifying p, width and height.
*/
def.static.PosWidthHeight =
	function( pos, width, height )
{
	return Self.create( 'pos', pos, 'width', width, 'height', height );
};

/*
| South point.
*/
def.lazy.ps =
	function( )
{
	return this.pos.add( this.width / 2, this.height );
};

/*
| West point.
*/
def.lazy.pw =
	function( )
{
	return this.pos.add( 0, this.height / 2 );
};

/*
| The path of the ellipse.
*/
def.lazy.path =
	function( )
{
	return(
		Path.Plan(
			'pc', this.pc,
			'start', this.pw,
			'qbend', this.pn,
			'qbend', this.pe,
			'qbend', this.ps,
			'qbend', 'close',
		)
	);
};

/*
| Returns a moved ellipse.
*/
def.proto.sub =
	function( a0, a1 )
{
	return this.create( 'pos', this.pos.sub( a0, a1 ) );
};

/*
| Returns a transformed ellipse.
*/
def.proto.transform =
	function( transform )
{
/**/if( CHECK && !TransformBase.isa( transform ) ) throw new Error( );

	// FUTURE, this creates gradientSettings even if there were none...
	//         since they fall back on defaults
	return(
		this.create(
			'pos', this.pos.transform( transform ),
			'width', transform.d( this.width ),
			'height', transform.d( this.height ),
			'gradientPC',
				this.gradientPC !== undefined
				? this.gradientPC.transform( transform )
				: pass,
			'gradientR0',
				this.gradientR0 !== undefined
				? transform.d( this.gradientR0 )
				: pass,
			'gradientR1',
				this.gradientR1 !== undefined
				? transform.d( this.gradientR1 )
				: pass
		)
	);
};

/*
| Returns true if p is within the ellipse.
*/
def.proto.within =
	function( p )
{
/**/if( CHECK && arguments.length !== 1 ) throw new Error( );

	return Display.helper.within( p, this );
};

/*
| Shortcut to create an ellipse by zone.
*/
def.static.Zone =
	function( zone )
{
	return(
		Self.create(
			'pos', zone.pos,
			'width', zone.width,
			'height', zone.height
		)
	);
};
